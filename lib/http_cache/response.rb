module Faraday
  class HttpCache < Faraday::Middleware
    # Internal: a class to represent a response from a Faraday request.
    # It decorates the response hash into a smarter object that queries
    # the response headers and status informations about how the caching
    # middleware should handle this specific response.
    class Response
      def cacheable?(unused_argument)
        return false if cache_control.no_store?

        cacheable_status_code? && (validateable? || fresh?)
      end
    end
  end
end