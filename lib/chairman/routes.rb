module Chairman
  class Routes < Sinatra::Base
    set :protection, except: :frame_options

    get '/authorize' do
      redirect to Chairman.session.authorize_url(Chairman.client_id, scope: Chairman.scope.join(','))
    end

    get '/authorize/return' do
      halt if params[:error] == 'access_denied'

      token = Chairman.session.exchange_code_for_token(params[:code], Chairman.client_id, Chairman.client_secret, {accept: 'application/json'})

      # TODO: Verify that returned scope is the same as we requested
      # See: http://developer.github.com/changes/2013-10-04-oauth-changes-coming/

      @user = Chairman.session(token.access_token).user

      session[:github_token] = token.access_token
      session[:github_id] = @user.login
    end

    get '/logout' do
      session[:github_token] = nil
      session[:github_id] = nil
    end
  end
end

