require 'sinatra/base'
require 'faraday/http_cache'
require 'http_cache/response'
require 'octokit'
require 'logger'
require 'oj'
require 'dalli'
require 'memcachier'
require 'chairman/routes'

module Chairman
  class << self
    attr_reader :session, :client_id, :client_secret, :scope

    def config(client_id, client_secret, scope)
      memcache_host = ENV['MEMCACHIER_SERVERS'] || 'localhost:11211'

      stack = Faraday::RackBuilder.new do |builder|
        builder.use Faraday::HttpCache, shared_cache: false, logger: Logger.new(STDOUT), store: :mem_cache_store, store_options: [memcache_host], serializer: Oj
        builder.adapter Faraday.default_adapter
      end
      Octokit.middleware = stack

      @client_id = client_id
      @client_secret = client_secret
      @scope = scope
    end

    def session(auth_token = nil)
      @session = Octokit::Client.new :access_token => auth_token, :client_id => @client_id, :client_secret => @client_secret
    end
  end
end

