$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "version"

Gem::Specification.new do |s|
  # Release Specific Information
  s.version     = Chairman::VERSION
  s.date = "2014-09-14"

  # Gem Details
  s.name = "chairman"
  s.authors = ["Jed Foster"]
  s.summary = %q{GitHub authentication for Sinatra}
  s.description = %q{GitHub authentication routes and tools for Sinatra apps}
  s.email = "jed@jedfoster.com"
  s.homepage = "https://github.com/jedfoster/Chairman"

  # Gem Files
  s.files = %w(README.md LICENSE)
  s.files += Dir.glob("lib/**/*.*")
  s.require_paths = ["lib"]

  # Gem Bookkeeping
  s.rubygems_version = %q{1.3.6}
  s.add_dependency("sinatra", ["~> 1.4.3"])
  s.add_dependency("octokit", ["~> 3.0"])
  s.add_dependency("faraday-http-cache", ["~> 0.4.0"])
  s.add_dependency("dalli", ["~> 2.6.4"])
  s.add_dependency("memcachier", ["~> 0.0.2"])
  s.add_dependency("oj", ["~> 2.4.0"])
end
